/*

[x] open kryptoraha.ee
[x] close cookie notification
[x] title is "Eesti Krüptoraha Liit - Uudised ja ettevõtlus"
[x] võta screenshot
[x] page contains header button in the header-section
[x] page contains link: "Uudised"
[x] klikkides lingi peale avaneb uus sait pealkirjaga: "Uudised | Eesti Krüptoraha Liit".
[x] page contains title "Uudised | Eesti Krüptoraha Liit"
[x] võta screenshot
[x] page contains header button in the header-section
[x] page contains link: "Top 10"
[x] klikkides lingi peale avaneb uus sait top 10 kryptovaluutat.
[x] võta screenshot
[x] page contains header input(type="text") ienter 'mta'
[x] 'mta' peale avaneb uus sait otsingutulemustega
[x] võta screenshot
*/

const config = require('../../nightwatch.conf.js');

module.exports = {

  'Eesti Krüptoraha Liit': function (browser) {
    browser
      .url('https://www.kryptoraha.ee/')
      .pause(2000)
      .waitForElementVisible('body')
      .waitForElementPresent('#cn-close-notice')
      .click('#cn-close-notice')
      .pause(2000)
      .assert.title('Eesti Krüptoraha Liit - Uudised ja ettevõtlus')
      .saveScreenshot(`${config.imgpath(browser)}krypto-main-page.png`)
      .useXpath()
      .assert.elementPresent('/html/body/div[1]/div[1]/header/div[3]/div/div/div/div[2]/a/i')
      .click('/html/body/div[1]/div[1]/header/div[3]/div/div/div/div[2]/a/i')
      .pause(2000)
      .assert.containsText('/html/body/div[2]/div[4]/nav/ul/li[2]/a/span', 'Uudised')
      .click('/html/body/div[2]/div[4]/nav/ul/li[2]/a/span')
      .assert.title('Uudised | Eesti Krüptoraha Liit')
      .saveScreenshot(`${config.imgpath(browser)}krypto-uudised.png`)
      .pause(2000)
      .assert.elementPresent('/html/body/div[1]/div[1]/header/div[3]/div/div/div/div[2]/a/i')
      .click('/html/body/div[1]/div[1]/header/div[3]/div/div/div/div[2]/a/i')
      .pause(2000)
      .assert.containsText('/html/body/div[2]/div[4]/nav/ul/li[3]/a/span', 'Top 10')
      .click('/html/body/div[2]/div[4]/nav/ul/li[3]/a/span')
      .saveScreenshot(`${config.imgpath(browser)}krypto-valuutad.png`)
      .pause(2000)
      .setValue('/html/body/div[1]/div[2]/div/div[2]/div/div/aside[1]/form/input[1]', 'mta')
      .saveScreenshot(`${config.imgpath(browser)}krypto-maksud.png`)
      .pause(8000)
      .end();
  },
};
