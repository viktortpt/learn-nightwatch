/*

[x] open delfi.ee
[x] title is "Delfi"
[x] võta screenshot
[x] page contains header button in the header-section
[x] page contains link: "Delfi Tasku"
[x] klikkides lingi peale avaneb uus sait pealkirjaga: "Tasku".
[x] page contains title "Tasku"
võta screenshot

*/

const config = require('../../nightwatch.conf.js');

module.exports = {

  'Delfi Tasku': function (browser) {
    browser
      .url('https://www.delfi.ee/')
      .pause(9000)
      .waitForElementVisible('body')
      .assert.title('Delfi')
      .saveScreenshot(`${config.imgpath(browser)}delfi-main-page.png`)
      .useXpath()
      .assert.elementPresent('//*[@id="__layout"]/div/header/div/nav[2]/div/div/div/div/div[3]/div/div/div/ul/li[7]/button')
      .click('//*[@id="__layout"]/div/header/div/nav[2]/div/div/div/div/div[3]/div/div/div/ul/li[7]/button')
      .saveScreenshot(`${config.imgpath(browser)}delfi-c-button.png`)
      .pause(1000)
      .assert.containsText('//*[@id="__layout"]/div/header/div/div[1]/div[1]/div/div/div[4]/div/div[1]/ul/li[9]/a', 'Delfi Tasku')
      .click('//*[@id="__layout"]/div/header/div/div[1]/div[1]/div/div/div[4]/div/div[1]/ul/li[9]/a')
      .assert.title('Tasku')
      .saveScreenshot(`${config.imgpath(browser)}delfi-tasku.png`)
      .pause(1000)

      .end();
  },
};
